const http = require('http');
const url  = require('url');
const fs = require('fs');
const { TemplateEngine } = require('thymeleaf');
const promParser = require('./parser/prom.js');
const epicentrParser = require('./parser/epicentr.js');
const rozetkaParser = require('./parser/rozetka.js');

const PORT = process.env.PORT || 3000;
const templateEngine = new TemplateEngine();

const getAllData = async (req, res) => {
  const jobs = [
    promParser.parse(),
    epicentrParser.parse(),
    rozetkaParser.parse()
  ];
  const data = (await Promise.all(jobs)).reduce((res, curValue) => res.concat(curValue), []);
  res.setHeader('Content-Type', 'application/json');
  res.statusCode = 200;
  res.end(JSON.stringify(data));
};

const sendFile = (file, type, res) => {
  const stream = fs.createReadStream(file);
  stream.on('open', () => {
     res.setHeader('Content-Type', type);
     stream.pipe(res);
  });
  stream.on('error', () => {
     res.setHeader('Content-Type', 'text/plain');
     res.statusCode = 404;
     res.end('Not found');
  });
};

const itemComparator = (e1, e2) => {
  if (e1.price === e2.price) {
    return 0;
  }
  return e1.price > e2.price ? 1 : -1;
};

const filterData = (data, params) => {
  params.products = +params.products;
  if (params.products && params.products > 0) {
    data = data.slice(0, params.products);
  }
  if (params.order === 'desc') {
    data = data.reverse();
  }
  return data.map(item => ({ ...item, price: item.price + ' UAN' }));
};

const renderHomePage = async (req, res) => {
  const jobs = [
    promParser.parse(),
    epicentrParser.parse(),
    rozetkaParser.parse()
  ];
  const data = (await Promise.all(jobs))
    .reduce((res, curValue) => res.concat(curValue), [])
    .map(item => ({ ...item, price: +item.price }))
    .sort(itemComparator);
  const dataLength = data.length;
  const params = url.parse(req.url, true).query;
  const page = await templateEngine.processFile('./public/index.html', { data: filterData(data, params), dataLength });
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/html;charset=UTF-8');
  res.end(page);
};

const routing = {
  '/': getAllData,
  '/home': renderHomePage
};

const server = http.createServer(async (req, res) => {
  console.log(req.method + ' ' + req.url);
  const path = url.parse(req.url, true).pathname;
  const fn = routing[path];
  if (fn) {
    await fn(req, res);
  } else if (path.startsWith('/css')) {
    sendFile('./public' + path, 'text/css', res);
  } else {
    res.statusCode = 404;
    res.setHeader('Content-Type', 'text/plain');
    return res.end('Resource you are looking for not found!');
  }
});

server.listen(PORT, () => console.log(`Server running at port ${PORT}`));

server.on('error', (err) => {
  if (err.code === 'EACCES') {
    console.log(`No access to port: ${PORT}`);
  }
});
