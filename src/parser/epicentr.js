const fs = require('fs');
const xpath = require('xpath');
const dom = require('xmldom').DOMParser;
const fetch = require('node-fetch');

// const img = xpath.select('/html/body/main/section[2]/div[2]/div[2]/div/div[1]/div[1]/div/div/a/img', doc);
// const title = xpath.select('/html/body/main/section[2]/div[2]/div[2]/div/div[1]/div[1]/div/div/div[3]/a/b', doc);
// const price = xpath.select('/html/body/main/section[2]/div[2]/div[2]/div/div[1]/div[1]/div/div/div[4]/div[1]/p/span', doc);
const parse = async () => {
  // const file = fs.readFileSync('./epicentrk.html', { encoding: 'utf8' });
  const fileRes = await fetch('https://epicentrk.ua/ua/shop/krupy-i-makaronnye-izdeliya/fs/vid-krupa-grechnevaya/');
  const file = await fileRes.text();
  const doc = new dom().parseFromString(file);
  const itemsCount = xpath.select('/html/body/main/section[2]/div[3]/div[2]/div/div[1]/div', doc).length;
  const data = [];
  for (let i = 1; i <= itemsCount; i++) {
    try {
      const img = xpath.select(`/html/body/main/section[2]/div[3]/div[2]/div/div[1]/div[${i}]/div/div/a/img`, doc)[0].attributes[0].value;
      const title = xpath.select(`/html/body/main/section[2]/div[3]/div[2]/div/div[1]/div[${i}]/div/div/div[3]/a/b`, doc)[0].firstChild.data.trim();
      const price = xpath.select(`/html/body/main/section[2]/div[3]/div[2]/div/div[1]/div[${i}]/div/div/div[4]/div[1]/p/span`, doc)[0].firstChild.data.trim();
      const url = xpath.select(`/html/body/main/section[2]/div[3]/div[2]/div/div[1]/div[${i}]/div/div/a`, doc)[0].attributes[1].value;
      data.push({ img, title, price, url: 'https://epicentrk.ua' + url });
    } catch (e) {
    }
  }
  return data;
}

module.exports = {
  parse
};
