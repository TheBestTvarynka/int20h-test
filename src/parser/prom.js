const fs = require('fs');
const xpath = require('xpath');
const dom = require('xmldom').DOMParser;
const fetch = require('node-fetch');

const parse = async () => {
  // const file = fs.readFileSync('./prom.html', { encoding: 'utf8' });
  const fileRes = await fetch('https://prom.ua/Krupa-grechnevaya.html?category=20521');
  const file = await fileRes.text();
  const doc = new dom().parseFromString(file);
  const nodes = xpath.select('/html/body/div[1]/div[1]/div[1]/div/div[3]/div[3]/div[2]/div/div[2]/div[1]/div[1]/div/div/div/div[1]/script', doc);
  const data = [];
  for (const node of nodes) {
    try {
      const item = JSON.parse(node.childNodes[0].data);
      data.push({ img: item.image[0], title: item.name, price: item.offers.price, url: item.url });
    } catch (e) {
      // console.log(e);
    }
  }
  return data;
}

module.exports = {
  parse
};
