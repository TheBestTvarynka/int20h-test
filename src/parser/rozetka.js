const fs = require('fs');
const xpath = require('xpath');
const dom = require('xmldom').DOMParser;
const fetch = require('node-fetch');

const parse = async () => {
  // const file = fs.readFileSync('./rozetka.html', { encoding: 'utf8' });
  const fileRes = await fetch('https://rozetka.com.ua/ua/krupy/c4628397/vid-225787=grechka/');
  const file = await fileRes.text();
  const doc = new dom().parseFromString(file);
  const data = [];
  for (let i = 1; i <= 60; i++) {
    const img = xpath.select(`/html/body/app-root/div/div[1]/rz-category/div/main/rz-catalog/div/div/section/rz-grid/ul/li[${i}]/app-goods-tile-default/div/div[2]/a[1]/img[2]`, doc)[0].attributes[2].value;
    const title = xpath.select(`/html/body/app-root/div/div[1]/rz-category/div/main/rz-catalog/div/div/section/rz-grid/ul/li[${i}]/app-goods-tile-default/div/div[2]/a[2]/span`, doc)[0].childNodes[1].data.trim();
    const price = xpath.select(`/html/body/app-root/div/div[1]/rz-category/div/main/rz-catalog/div/div/section/rz-grid/ul/li[${i}]/app-goods-tile-default/div/div[2]/div[4]/div[2]/p/span[1]`, doc)[0].firstChild.data.trim();
    const url = xpath.select(`/html/body/app-root/div/div[1]/rz-category/div/main/rz-catalog/div/div/section/rz-grid/ul/li[${i}]/app-goods-tile-default/div/div[2]/a[2]`, doc)[0].attributes[2].value;
    data.push({ img, title, price, url });
  }
  return data;
};

module.exports = {
  parse
};
